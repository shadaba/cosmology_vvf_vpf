# Intro:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/shadaba%2Fcosmology_vvf_vpf/master?filepath=Abacus-cosmo-LSS.ipynb)

This notebook provide basic function to load the parameters and measurements and make some preliminary plots.
We are using Abacus Nbody simulation to study the ability to cosmological parameter sensitivity on various observables. You can learn more about Abacus suites of Nbody simulation here: https://lgarrison.github.io/AbacusCosmos/simulations/

We have taken the 720 Mpc/h box from Abaxus suit and created mock galaxy catalogue representing GAMA sample at low redshift with r band absolute limit of -21. The 40 different cosmology are populated and should be used for this study. The over all goal will to understand the information content in different observable for which we will use fisher technique. Please read section 5 of https://arxiv.org/pdf/1911.11158.pdf paper for description of how such method can be used.

Our longer term goal will be to use projected correlation function (wp), multipole moments (xi_ell), Vornois Volume Function (VVF) and Void probability function (VPF) in this exrcise. 

Currently we have wp, and xis024 available which will be our starting point.

So, the notebook provide how to access, load and plot these measurements including corresponding cosmological parameters.

The clustering file structure:
For each sim we assign a simname which is either planck or interget with two digit (00-39) and file names are:
<simaname>-wpxis024xi2dsmall-logr.txt
    
The file consists of mean measurement as well as jacknife realization. For a description of jackknife realizations please look at the tutorial on LSS:

The header of the file is as follows:
#wp xir-small-mu0 xi0 xir-small-mu1 xi2 xir-small-mu2 xi4 with jacknife NJN=225
#njn= 225
#nwp= 25
#nsxi= 6
#nssmall= 5
#r(Mpc/h) mean sigma All jacknifecolumns

