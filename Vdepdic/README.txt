This directory contains the fisher matrix forecast for Mr=-19 sample

The directory structure is described in the file:  dir_structure.txt 
A summary plot is given in the file: summary_plot.png

The first level sub-directory is simply the statistics used with following definition:
xi: clustering only (wp, xi0,2,4)
rhoxi: clustering + number density
rhoxivvf: rhoxi+ vvf
rhoxi_smin1: rho+ xi with smin of 1
rhoxivvf_smi1: rhoxivvf but with smin of 1 for xi

Under these you will first find subd-recotyr with various values of volume.
For the fisher ignoring parameter dependent covariance the relative result will be volume independet but the absolute value of error will depend on teh volume.
For any of the volume sub-diretory, you will find Fmat directory containing the fisher matrix which will contain two file
  a)fix_bfit.dat: standard fisher
  b)pdep.dat : fisher with parameter dependent covariance term
  Each of these file contains a nxn matrix which can be loaded by np.loadtxt(fielname).
  The size of the matrix is same as the number of parameter and the order of parameters is given in the file: hod_par_list.txt 

